import { Service } from 'egg';

/**
 * Test Service
 */
export default class Test extends Service {

  public async getList() {

    const data = await this.app.mysql.select('users');
    return {
      errCode: 0,
      data,
    };
  }
}
